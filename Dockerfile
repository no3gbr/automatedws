FROM php:8.0-apache-bullseye

# Installation et configuration des dépendances

RUN set -eux; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
		a2enmod ssl; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		git \
		curl \
		wget \
		nano \
		libcurl4-openssl-dev \
		libfreetype6-dev \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
		libzip-dev \
		libonig-dev \
		libedit-dev \
		libxml2-dev \
		default-mysql-client \
    ; \
	docker-php-ext-configure gd \
		--with-freetype \
		--with-jpeg=/usr \
	; \
	\
	docker-php-ext-install -j "$(nproc)" \
		gd \
		intl \
		opcache \
		mysqli \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
	\
	rm -rf /var/lib/apt/lists/*

RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Installation de composer


# Installation de composer

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN ln -s /usr/local/bin/composer /usr/bin/composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer


# Copie des fichiers
EXPOSE 80
EXPOSE 443
COPY /conf/woom.conf /etc/apache2/sites-available/000-default.conf
COPY /conf/woom.crt /etc/ssl/certs/woom.crt
COPY /conf/woom.key /etc/ssl/private/woom.key
COPY ./ /var/www/html/
WORKDIR /var/www/html/drupal/

